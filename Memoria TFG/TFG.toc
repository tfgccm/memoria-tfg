\babel@toc {spanish}{}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.1}% 
\contentsline {chapter}{Resumen}{\es@scroman {xi}}{chapter*.2}% 
\contentsline {paragraph}{Palabras clave:}{\es@scroman {xi}}{section*.3}% 
\contentsline {chapter}{Abstract}{\es@scroman {xiii}}{chapter*.4}% 
\contentsline {paragraph}{Keywords:}{\es@scroman {xiii}}{section*.5}% 
\contentsline {chapter}{\IeC {\'I}ndice}{\es@scroman {xvii}}{chapter*.6}% 
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n del proyecto}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Objetivos}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Materiales Utilizados}{3}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Estructura del documento}{4}{section.1.4}% 
\contentsline {section}{\numberline {1.5}Abreviaturas y Acr\IeC {\'o}nimos}{5}{section.1.5}% 
\contentsline {chapter}{\numberline {2}Estado del arte}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Dom\IeC {\'o}tica y aplicaciones}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Evoluci\IeC {\'o}n hist\IeC {\'o}rica de la Dom\IeC {\'o}tica}{8}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Dom\IeC {\'o}tica en el \IeC {\'a}mbito de la Seguridad}{10}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2.2}Integraci\IeC {\'o}n de la dom\IeC {\'o}tica en \textit {Internet of Things}}{12}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Descripci\IeC {\'o}n del Hardware desarrollado}{15}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Placa Microcontroladora: Arduino UNO}{15}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Dispositivo Gateway: Raspberry Pi 3 B}{17}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Sensores y Actuadores}{18}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Sensor de Temperatura y Humedad}{18}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Sensor de Detecci\IeC {\'o}n de Movimiento}{19}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Sensor de Detecci\IeC {\'o}n de Gases}{20}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Sensor de Detecci\IeC {\'o}n de Inundaci\IeC {\'o}n}{22}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Zumbador de Alarma}{22}{subsection.3.3.5}% 
\contentsline {subsection}{\numberline {3.3.6}C\IeC {\'a}mara de vigilancia}{23}{subsection.3.3.6}% 
\contentsline {section}{\numberline {3.4}Diagrama de Conexiones}{25}{section.3.4}% 
\contentsline {section}{\numberline {3.5}Maqueta}{26}{section.3.5}% 
\contentsline {chapter}{\numberline {4}Dise\IeC {\~n}o de la Arquitectura}{31}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Introducci\IeC {\'o}n}{31}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Arquitectura Hardware}{32}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Arquitectura Software}{34}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Programa Arduino}{34}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Programa Gateway}{35}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Comunicaci\IeC {\'o}n Arduino - Raspberry Pi}{38}{subsection.4.3.3}% 
\contentsline {subsection}{\numberline {4.3.4}Aplicaci\IeC {\'o}n API REST}{41}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Aplicaci\IeC {\'o}n Android}{43}{subsection.4.3.5}% 
\contentsline {subsection}{\numberline {4.3.6}Servidor de V\IeC {\'\i }deo Streaming}{49}{subsection.4.3.6}% 
\contentsline {chapter}{\numberline {5}Descripci\IeC {\'o}n del Software desarrollado}{51}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Programa de Control}{51}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}M\IeC {\'e}todo \textit {setup()}}{52}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}M\IeC {\'e}todo \textit {loop()}}{54}{subsection.5.1.2}% 
\contentsline {section}{\numberline {5.2}Programa Gateway}{58}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Hilo principal}{59}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Hilo IP p\IeC {\'u}blica}{65}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Hilo de autenticaci\IeC {\'o}n}{66}{subsection.5.2.3}% 
\contentsline {subsection}{\numberline {5.2.4}Hilo de refresco de token}{67}{subsection.5.2.4}% 
\contentsline {section}{\numberline {5.3}Aplicaci\IeC {\'o}n API REST}{68}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Modelo de datos}{72}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}Permisos}{76}{subsection.5.3.2}% 
\contentsline {subsection}{\numberline {5.3.3}URLs}{76}{subsection.5.3.3}% 
\contentsline {subsection}{\numberline {5.3.4}Vistas}{78}{subsection.5.3.4}% 
\contentsline {subsection}{\numberline {5.3.5}Serializador}{80}{subsection.5.3.5}% 
\contentsline {subsection}{\numberline {5.3.6}Autenticaci\IeC {\'o}n - JSON Web Tokens}{82}{subsection.5.3.6}% 
\contentsline {subsection}{\numberline {5.3.7}Firebase Cloud Messages - Servidor}{85}{subsection.5.3.7}% 
\contentsline {subsection}{\numberline {5.3.8}Servicios}{89}{subsection.5.3.8}% 
\contentsline {subsection}{\numberline {5.3.9}Documentaci\IeC {\'o}n}{90}{subsection.5.3.9}% 
\contentsline {section}{\numberline {5.4}Aplicaci\IeC {\'o}n M\IeC {\'o}vil}{92}{section.5.4}% 
\contentsline {subsection}{\numberline {5.4.1}Recursos}{97}{subsection.5.4.1}% 
\contentsline {subsubsection}{Drawable}{97}{section*.88}% 
\contentsline {subsubsection}{Layout}{97}{section*.90}% 
\contentsline {subsubsection}{Menu}{99}{section*.95}% 
\contentsline {subsubsection}{Values}{100}{section*.96}% 
\contentsline {subsection}{\numberline {5.4.2}Utilidades}{102}{subsection.5.4.2}% 
\contentsline {subsection}{\numberline {5.4.3}Cliente REST}{102}{subsection.5.4.3}% 
\contentsline {subsection}{\numberline {5.4.4}Actividades}{108}{subsection.5.4.4}% 
\contentsline {subsubsection}{Acceso}{112}{section*.103}% 
\contentsline {subsubsection}{Registro}{115}{section*.107}% 
\contentsline {subsubsection}{Men\IeC {\'u} Principal y Men\IeC {\'u} Superior}{116}{section*.110}% 
\contentsline {subsubsection}{Notificaciones}{119}{section*.114}% 
\contentsline {subsubsection}{Temperatura y Humedad}{121}{section*.116}% 
\contentsline {subsubsection}{C\IeC {\'a}mara de Seguridad}{122}{section*.119}% 
\contentsline {subsubsection}{Listado de Alarma}{124}{section*.122}% 
\contentsline {subsubsection}{Configuraci\IeC {\'o}n}{126}{section*.125}% 
\contentsline {chapter}{\numberline {6}Pruebas y resultados}{129}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Pruebas}{129}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Resultados}{134}{section.6.2}% 
\contentsline {chapter}{\numberline {7}Gesti\IeC {\'o}n del proyecto}{137}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Planificaci\IeC {\'o}n}{137}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Metodolog\IeC {\'\i }a de trabajo}{139}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Almacenamiento y Control de Versiones}{139}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}Seguimiento del proyecto}{140}{subsection.7.2.2}% 
\contentsline {section}{\numberline {7.3}Presupuesto}{142}{section.7.3}% 
\contentsline {chapter}{\numberline {8}Conclusiones}{143}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Conclusi\IeC {\'o}n}{143}{section.8.1}% 
\contentsline {section}{\numberline {8.2}Desarrollos futuros}{144}{section.8.2}% 
\contentsline {chapter}{\numberline {A}Hojas de Caracter\IeC {\'\i }sticas}{147}{appendix.Alph1}% 
\contentsline {chapter}{\numberline {B}Manuales de Instalaci\IeC {\'o}n y Configuraci\IeC {\'o}n}{163}{appendix.Alph2}% 
\contentsline {section}{\numberline {B.1}Instalaci\IeC {\'o}n de Raspbian}{163}{section.Alph2.1}% 
\contentsline {section}{\numberline {B.2}Instalaci\IeC {\'o}n Programa Gateway}{166}{section.Alph2.2}% 
\contentsline {section}{\numberline {B.3}Instalaci\IeC {\'o}n y Configuraci\IeC {\'o}n Servidor UV4L}{167}{section.Alph2.3}% 
\contentsline {section}{\numberline {B.4}Instalaci\IeC {\'o}n Servidor Django Rest Framework}{171}{section.Alph2.4}% 
\contentsline {section}{\numberline {B.5}Instalaci\IeC {\'o}n Aplicaci\IeC {\'o}n Android SIDOCS}{177}{section.Alph2.5}% 
\contentsline {chapter}{\numberline {C}Manual de Usuario - Aplicaci\IeC {\'o}n M\IeC {\'o}vil SIDOCS}{179}{appendix.Alph3}% 
\contentsline {section}{\numberline {C.1}Introducci\IeC {\'o}n}{179}{section.Alph3.1}% 
\contentsline {section}{\numberline {C.2}Requisitos del Sistema}{179}{section.Alph3.2}% 
\contentsline {section}{\numberline {C.3}M\IeC {\'o}dulos de la Aplicaci\IeC {\'o}n}{179}{section.Alph3.3}% 
\contentsline {subsection}{\numberline {C.3.1}Registro}{179}{subsection.Alph3.3.1}% 
\contentsline {subsection}{\numberline {C.3.2}Acceso}{181}{subsection.Alph3.3.2}% 
\contentsline {subsection}{\numberline {C.3.3}Men\IeC {\'u} Principal}{181}{subsection.Alph3.3.3}% 
\contentsline {subsection}{\numberline {C.3.4}Temperatura y Humedad}{182}{subsection.Alph3.3.4}% 
\contentsline {subsection}{\numberline {C.3.5}C\IeC {\'a}mara de Seguridad}{183}{subsection.Alph3.3.5}% 
\contentsline {subsection}{\numberline {C.3.6}Listado de Alarmas}{183}{subsection.Alph3.3.6}% 
\contentsline {subsection}{\numberline {C.3.7}Configuraci\IeC {\'o}n}{184}{subsection.Alph3.3.7}% 
\contentsline {subsection}{\numberline {C.3.8}Notificaciones}{185}{subsection.Alph3.3.8}% 
\contentsline {subsection}{\numberline {C.3.9}Men\IeC {\'u} Superior}{186}{subsection.Alph3.3.9}% 
\contentsline {chapter}{Bibliografia}{187}{figure.caption.168}% 
