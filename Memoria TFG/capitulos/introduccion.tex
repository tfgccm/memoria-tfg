\chapter{Introducción}


\section{Motivación del proyecto}

En la actualidad, el mercado de la domótica ofrece gran variedad de productos para cubrir las necesidades cotidianas del ser humano.
De forma particular, los sistemas de seguridad domóticos a menudo aportan una solución con funcionalidades limitadas. De este modo, si se desea disponer de una protección completa en el hogar, se suele incurrir en un alto coste para el usuario final, con pagos de cuotas mensuales o anuales al proveedor del servicio.

Al mismo tiempo existen frecuentes problemas de compatibilidad entre los productos de distintos fabricantes al no emplearse un protocolo de comunicación estándar y libre.
\\

En este escenario surge \textbf{SIDOCS}, acrónimo de "\textit{Sistema Domótico de Control y Seguridad}", que tiene por objeto ofrecer una solución completa a los problemas de seguridad más comunes del hogar y, frente a los sistemas existentes, aportar las siguientes mejoras:

\begin{itemize}
	\item Un arquitectura basada en hardware y software libre.
	\item Un canal seguro de comunicación sensor/actuador-servidor-cliente.
	\item Bajo coste, accesible a mayor número de usuarios que el resto de soluciones del mercado.
	\item Diseño que favorece la escalabilidad y extensibilidad de las funcionalidades del sistema, hacia otras aplicaciones de la domótica.
\end{itemize}

\vfill

\pagebreak

\section{Objetivos}

%Es muy importante señalar el objetivo principal del TFG, así como los objetivos secundarios que se estableciaron al %principio o han ido surgiendo durante su elaboración.

El objetivo principal del proyecto consiste en el diseño y desarrollo de un sistema domótico de seguridad, que permita su monitorización y configuración por medio de una aplicación móvil.

Para una mejor comprensión, a continuación se desglosa en seis puntos el objetivo principal del proyecto:

\begin{enumerate}
	\item \textbf{Diseño y desarrollo de un programa de Control}, que empleando el microcrontrolador Arduino, permitirá la gestión de la comunicación por puerto serie con el ordenador Raspberry Pi y el control de los parámetros: temperatura, humedad relativa y alarmas producidas por presencia de gases tóxicos, inundación y allanamiento del hogar. 
	
	%\begin{itemize} 
	%	\item DHT11: para la medición de la temperatura y humedad relativa del hogar.
	%	\item Sensor de nivel de agua: que permitirá la detección de inundaciones.
	%	\item MQ-2: usado en la medida de la concentración de gases, permite la detección de incendios y fugas de gas.
		%\item MQ-2: para la detección de incendios por presencia de humo, y fugas de gas al percibir concentraciones elevadas de LPG, butano o propano.
	%	\item PIR: permite la detección de movimiento y por consiguiente, un allanamiento en el hogar.
	%	\item Zumbador: para señalizar de manera acústica las alertas.
		%\item Zumbador: señal acústica para alertar a usuarios y/o vecinos de la existencia de una amenaza en el hogar.
	%	\item Diodos LED: dirigidos a la señalización visual de la activación de alarmas.
	%\end{itemize}

	\item \textbf{Instalación y  configuración del Servidor \textit{streaming UV4L}} en Raspberry Pi. Reproducirá en tiempo real la imagen capturada por la cámara de vigilancia sobre el protocolo seguro HTTPS.
	
	\item \textbf{Diseño y desarrollo de un programa \textit{Gateway}}, que programado en lenguaje \textit{Python}, se ejecutará desde Raspberry Pi y gestionará el tráfico de comunicaciones entre Arduino y la aplicación API REST del servidor web.

	\item \textbf{Diseño y desarrollo de una aplicación API REST} usando la librería \textit{Django Rest Framework}. Ésta se alojará en un servidor web en la nube, permitiendo un acceso global y seguro a los datos almacenados en la base de datos del sistema. Así mismo, facilitará la gestión de usuarios, roles, permisos y servicios externos (correo electrónico, notificaciones FCM,...).
	
	\item \textbf{Diseño y desarrollo de una aplicación móvil Android} que permitirá al usuario consultar la temperatura y humedad actual del hogar, recibir notificaciones de alarmas y acceder a su listado histórico, visualizar la emisión de la cámara de vigilancia en tiempo real y modificar la configuración del sistema.
	
	\item \textbf{Diseño y construcción de una maqueta a escala} de una vivienda a fin de representar un entorno real de funcionamiento del sistema.
	

\end{enumerate}

\vfill

\pagebreak

\section{Materiales Utilizados}
Para el desarrollo de la arquitectura final del proyecto, se precisó de una serie de componentes hardware y software. A continuación se muestra un listado con los principales materiales empleados:

\begin{itemize}
	\item Control y Procesamiento
		\begin{itemize}
			\item Arduino UNO
			\item Raspberry Pi 3 Modelo B
			\item Servidor de alojamiento web en la nube, \textit{PythonAnywhere}
		\end{itemize}
	\item Sensores y Actuadores
		\begin{itemize}
		\item Cámara Pi NoIR V2
		\item Focos LED IR con sensor LDR
		\item Sensor DHT22
		\item Sensor PIR
		\item Sensor MQ-2
		\item Sensor de agua
		\item Zumbador
		\item Diodos LED
		
		\end{itemize}
\end{itemize}

\vfill

\pagebreak

\section{Estructura del documento}

La memoria del proyecto se ha estructurado en ocho capítulos, partiendo de una introducción inicial al proyecto y posteriormente desarrollando el resto de apartados que describirán el hardware y software empleado, el diseño del sistema, las pruebas y resultados extraídos, la planificación y metodología de trabajo seguida y el presupuesto del proyecto. 

Adicionalmente se adjuntan anexos, las hojas de características de los sensores y actuadores, los manuales de instalación y configuración del sistema y el manual de usuario de la aplicación móvil.

%A continuación y para facilitar la lectura del documento, se detalla el contenido de cada %capítulo.

\begin{itemize}
\item \textbf{Capítulo 1: Introducción.} En él se repasan los motivos que han impulsado la realización del proyecto, junto con los objetivos a alcanzar tras su consecución. De forma complementaria se especifica un listado de materiales empleados y un glosario de acrónimos usados a lo largo del documento.
\item \textbf{Capítulo 2: Estado del Arte.} Se desarrolla un contenido teórico que expone la evolución de la domótica hasta la actualidad y su transformación hacia el concepto IoT.
\item \textbf{Capítulo 3: Descripción del Hardware desarrollado.} Se exponen los distintos componentes hardware empleados en el proyecto, así como los materiales y montaje de la maqueta.
\item \textbf{Capítulo 4: Diseño de la Arquitectura.} Se presenta el diseño escogido para la arquitectura hardware y software del proyecto, indicando los motivos que condujeron a su diseño final.
\item \textbf{Capítulo 5: Descripción del Software desarrollado.} En él se explican todos los detalles referentes a los componentes software desarrollados, incidiendo en los aspectos técnicos más relevantes.
\item \textbf{Capítulo 6: Pruebas y resultados.} Se detalla y ejecuta un protocolo de pruebas a fin de verificar que el proyecto ha alcanzado los objetivos marcados. Adicionalmente se desarrolla un comentario acerca de los resultados obtenidos.
\item \textbf{Capítulo 7: Gestión del Proyecto.} En este capítulo se expone la planificación estimada, la metodología adoptada para el seguimiento del proyecto y el presupuesto material.
\item \textbf{Capítulo 8: Conclusiones.} Se exponen las conclusiones extraídas de la realización del proyecto así como las posibles mejoras y evolutivos futuros.
\item \textbf{Apéndice A: Hojas de Características.} Se adjuntas las hojas técnicas de características de los sensores y actuadores hardware empleados, ofrecidos por los fabricantes del producto.
\item \textbf{Apéndice B: Manuales de Instalación y Configuración.} Se describen todos los pasos necesarios para configurar e instalar los paquetes requeridos en la puesta en marcha y funcionamiento del proyecto. 
\item \textbf{Apéndice C: Manual de Usuario - Aplicación Móvil SIDOCS.} Se expone un documento que servirá al usuario final de guia en el uso de la aplicación Android desarrollada.

\end{itemize}


\vfill

\pagebreak

\section{Abreviaturas y Acrónimos}

Para un mejor seguimiento y comprensión del documento por parte del lector, se describen en la tabla \ref{tab:tabla_acronimos} los acrónimos y abreviaturas empleados en la redacción del mismo:

\begin{table}[htbp]
	\caption{Abreviaturas y Acrónimos}
	\label{tab:tabla_acronimos}
	\begin{center}
		\begin{tabular}{|c||c|}
			\hline
			\textbf{Elemento} & \textbf{Descripción}\\
			\hline
			\hline
			API & Application Programming Interface\\
			APK & Android Application Package\\
			BBDD & Base de Datos\\
			CCD & Charge-Coupled Device\\
			CCTV & Closed-Circuit Television\\
			CRUD & Create - Read - Update - Delete\\
			DNS & Domain Name System\\
			DRF & Django Rest Framework\\
			FCM & Firebase Cloud Messaging\\
			FFC & Flexible Flat Cable\\
			GLP & Gas Licuado de Petróleo\\
			GPIO & General Purpose Input/Output\\
			GSM & Global System for Mobile communications\\
			HTTP & Hypertext Transfer Protocol\\
			HTTPS & Hypertext Transfer Protocol Secure\\
			ICR & Infrared Cutfilter Removal\\
			IDE & Integrated Development Environment\\
			IoT & Internet of Things\\\
			IP & Internet Protocol\\
			JSON & JavaScript Object Notation\\
			JWT & JSON Web Tokens\\
			ORM & Object-Relational Mapping\\
			PIR & Pyroelectric InfraRed sensor\\
			POJO & Plain Old Java Object\\			
			POO & Programación Orientada a Objetos\\
			PSU & Power Supply Unit\\
			REST & Representational State Transfer\\
			SDK & Software Development Kit\\
			SIDOCS & Sistema Domótico de Control y Seguridad\\
			SMS & Short Message Service\\
			SQL & Structured Query Language\\
			SSL & Secure Sockets Layer\\
			UI  & User Interface\\
			UPB & Universal Powerline Bus\\
			URL & Uniform Resource Locator\\
			WIP & Work In Progress\\
			WSGI & Web Server Gateway Interface\\
			XML & eXtensible Markup Language\\	
			
			\hline
		\end{tabular}
	\end{center}
\end{table}



