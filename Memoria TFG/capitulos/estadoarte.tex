\chapter{Estado del arte}
\label{chap:estadodelarte}

En este capítulo se expone al lector una visión de la evolución histórica de la domótica hasta su escenario actual, analizando de manera más concreta sus aplicaciones en el campo de la seguridad y su transformación dentro del contexto \textit{Internet of Things}.

\section{Domótica y aplicaciones}

Atendiendo a la definición establecida por la Real Academia de la Lengua Española (RAE), se entiende por domótica el \footnote{\url{https://dle.rae.es/?id=E7W0v9b}}\textit{conjunto de sistemas que automatizan las diferentes instalaciones de una vivienda}. Es por ello que su objetivo principal se centra en la mejora del confort, eficiencia energética, comunicaciones y  seguridad del ser humano en el ámbito doméstico.

Una de las aplicaciones más extendidas y populares en la domótica actual es el confort \cite{Nan:2017}. La gestión de la iluminación del hogar, controlar contenido multimedia o consultar la temperatura y humedad de una habitación de forma remota, son sólo algunos ejemplos de las funcionalidades que mejoran la comodidad del ser humano en su día a día. 

Por otro lado la domótica posibilita un incremento en la eficiencia energética de los hogares \cite{Salman:2016}. Tomando como referencia las rutinas de los usuarios y sus necesidades puntuales, se puede alcanzar no sólo una reducción del consumo energético, si no también un ahorro para la economía familiar. Todo ello es posible gracias al control automático de los sistemas de calefacción o el encendido y apagado de la iluminación y electrodomésticos.

La irrupción y evolución de los dispositivos móviles en la vida cotidiana del ser humano, ha propiciado un cambio radical en las formas de comunicación. En este escenario, la domótica crea una red de comunicaciones entre dispositivos, donde gracias a los paneles de control y aplicaciones móviles, el usuario puede monitorizar, controlar e interactuar con el sistema, de manera local y remota.
\vfill
\pagebreak

Otra de las aplicaciones que más protagonismo ha tomado en los últimos años es la seguridad. En este contexto, la domótica permite proteger los hogares y sus inquilinos, a partir de la monitorización, alerta y prevención de amenazas. 

Ejemplo de ello son los sistemas de simulación de presencia, cierre automático de válvulas de agua y gas ante fugas, sensores de detección de movimiento, cámaras que reproducen en tiempo real lo que ocurre en el interior del hogar y notificaciones en aplicaciones móviles para alertar al usuario.


\subsection{Evolución histórica de la Domótica}

El concepto ''domótica'' no es en absoluto moderno, ya a principios de siglo XX con la invención de los primeros aparatos eléctricos para el hogar como el aspirador y posteriormente la lavadora o la plancha eléctrica, se fijaron los cimientos hacia la concepción actual del término \cite{HASHistory}. No obstante, pese a suponer un gran avance en la mejora de las condiciones cotidianas del ser humano, estos aparatos no podrían considerarse dispositivos domóticos en el marco actual del término.  
\\

No fue hasta los años 60 cuando el ingeniero estadounidense James Sutherland diseñó y construyó el prototipo \textit{ECHO IV}, siglas de \textit{Electronic Computing Home Operator} (figura \ref{fig:echoiv}), considerado como el primer dispositivo domótico de la historia. Las funcionalidades que ofrecía suponían un gran salto tecnológico para la época pues permitía crear alarmas de reloj, ejecutar algunos controles básicos sobre el televisor, elaborar inventarios domésticos o crear música mediante un generador de tonos.\cite{ECHOIV}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.45\textwidth]{figuras/estadoarte/echoiv.jpg}
	\caption{Prototipo ECHO IV}{\protect\footnotemark}
	\label{fig:echoiv}
\end{figure}
	\footnotetext{Fuente: SmugMug, URL: \url{https://cortesi.smugmug.com/Other/ECHO-IV-article}}

\vfill
\pagebreak
Una década posterior, coincidiendo con el nacimiento del microprocesador, la empresa escocesa \textit{Pico Electronics} desarrolló el protocolo de comunicación \textit{X10}. Diseñado especialmente para la automatización y control remoto de dispositivos eléctricos, supuso el nacimiento del primer sistema domótico. 

Sin necesidad de instalación adicional por parte del usuario, el protocolo \textit{X10} hacía uso de la propia instalación eléctrica del hogar para establecer una comunicación binaria entre un panel de control y los aparatos eléctricos, mediante ráfagas de pulsos de 120kHz. Adicionalmente disponía de la capacidad de conectar al sistema eléctrico módulos de radiofrecuencia, que hacían posible la conexión inalámbrica con sensores y actuadores.

Pese a la extensa gama de productos que ofrecía X10 (control de iluminación, termostatos o alarmas antiintrusión entre otras) poseía numerosos inconvenientes fruto del medio de comunicación empleado: interferencias, perdida de mensajes, bajo ancho de banda, limitación en el número de dispositivos a conectar e inexistencia de encriptación de datos. \cite{X10:2015} No obstante, estos problemas fueron paulatinamente resueltos por otros sistemas y protocolos domóticos posteriores.
\\

En la década de los 90, \cite{HASevolution:2009} la domótica evolucionó de manera notable con la aparición de nuevos dispositivos y protocolos de comunicación como KNX o UPB, pero pese a los esfuerzos de los fabricantes en la reducción de los costes de producción y precio venta al usuario final, la cuota de mercado de los sistemas domóticos seguía siendo reducida.
\\

Con la entrada en la década del 2000, se produjeron cuatro acontecimientos fundamentales que explican en gran medida la expansión actual de la domótica:

	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.80\textwidth]{figuras/estadoarte/smarthome.jpg}
		\caption{Diagrama de sistema domótico en la actualidad}{\protect\footnotemark}
		%\caption*{Fuente: DogTownMedia}
		%\caption*{https://www.dogtownmedia.com/smart-home-arms-race-amazon-google}
		\label{fig:smarthome}
	\end{figure}
	\footnotetext{Fuente: DogTownMedia, URL: \url{https://www.dogtownmedia.com/smart-home-arms-race-amazon-google}}

\pagebreak
En primer lugar la mejora de los circuitos integrados, ya que con la reducción de su coste de producción y el incremento de sus prestaciones, se abrió la puerta a la fabricación de sensores y actuadores de bajo coste.

Por otro lado, el acceso generalizado a Internet por gran parte de la sociedad, y su integración como parte fundamental en la arquitectura de los sistemas domóticos. 

Los datos emitidos por los sensores ahora pueden ser procesados por servidores alojados en la nube y gracias a la evolución de dispositivos móviles como teléfonos inteligentes y tabletas, los usuarios pueden controlar y monitorizar el sistema mediante una conexión no sólo local, sino también remota.

Por último con la aparición de protocolos de comunicación inalámbricos basados en radiofrecuencia como Z-Wave o ZigBee. \cite{X10:2015} Su comunicación en red de malla que permitía intercambiar información entre dispositivos, unido a la incorporación de una capa de seguridad para encriptar mensajes, suponen un avance sustancial sobre muchos de los problemas inherentes a los protocolos precedentes.


\subsection{Domótica en el ámbito de la Seguridad}

La domótica ha conseguido transformar nuestra concepción de seguridad doméstica en las últimas dos décadas. 

Tradicionalmente los sistemas de seguridad domésticos estaban destinados, casi en exclusiva, a la detección de allanamientos mediante sensores de detección de movimiento, apertura de puertas, rotura de ventanas y CCTV. En la actualidad su campo de acción se ha ampliado y trasformado para proteger tanto el hogar como sus inquilinos. 

	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.40\textwidth]{figuras/estadoarte/camaraip.png}
		\caption{Cámaras IP de vigilancia}{\protect\footnotemark}
		%\caption*{Fuente: TP-Link}
		%\caption*{https://eu.dlink.com/es/es/empresas/camaras-ip}
		\label{fig:camaraip}
	\end{figure}
	\footnotetext{Fuente: TP-Link, URL: \url{https://eu.dlink.com/es/es/empresas/camaras-ip}}

Las necesidades del usuario han cambiado y por tanto se requieren sistemas integrales e interconectados que permitan monitorizar, prevenir, detectar y recibir alertas ante cualquier amenaza, ya sea provocada por agentes externos o internos \cite{HASsecurity:2015}.

En este escenario surgen toda suerte de sensores y actuadores, tales como simuladores de presencia por control automático de iluminación, detectores de fugas de gas e inundaciones conectados a electroválvulas que cierran el suministro general de la vivienda o cierres electrónicos de puertas.

\pagebreak
Al igual que en otras aplicaciones de la domótica, la integración con dispositivos móviles juega un papel fundamental. 
Ya no es necesaria la presencia del usuario en la vivienda para poder conocer de primera mano lo que ocurre en ella. Las cámaras IP de vigilancia con acceso a Internet otorgan una herramienta esencial en la monitorización del hogar de forma remota. Del mismo modo, la presencia real o inminente de amenazas en la vivienda podrán ser notificadas al usuario de manera instantánea por medio de los servicios de notificación push, mensajes SMS, o mensajes de correo electrónico \cite{HASIoT:2017}. 
\\

Un aspecto primordial de la domótica en general, y muy especialmente en la domótica de seguridad, es la encriptación y segurización de las comunicaciones. Los dispositivos de un sistema domótico recogen constantemente información del estado de la vivienda y la rutina de sus inquilinos, por lo que su intercepción por un atacante informático puede desembocar en un grave problema.

Tal y como mencionan Behrang Fouladi y Sahand Ghanoun durante su ponencia en 2013 ''\textit{Honey, I'm Home!! - Hacking Z-Wave Home Automation Systems}'' \cite{HoneyImHomeVideo}, la seguridad y encriptación de las comunicaciones se ha convertido en un aspecto de vital importancia a tratar por los proveedores de sistemas domóticos. La simple intercepción de un paquete de datos por un atacante, abre la puerta, no sólo a los datos enviados por todos los dispositivos conectados a la red doméstica, si no también a la posibilidad de interactuar con ellos.
\\

Con la proliferación de placas microcontroladoras y ordenadores de placa reducida con bajo coste y tecnología abierta, han surgido multitud de proyectos electrónicos de distinta naturaleza y aplicaciones.

De manera específica, en el ámbito de la domótica de seguridad caben destacar trabajos como el publicado en 2016 por el Instituto de Ciencia y Tecnología de la India \cite{HASMonitSec:2016}. El diseño establecía la conexión de un sensor de detección de movimiento PIR y un sensor de temperatura DHT22 a una placa Arduino UNO. La placa recogía las lecturas de ambos sensores y se enviaban alertas SMS a usuarios móviles, mediante un módulo GSM, cuando se detectaba un movimiento o cuando la temperatura excedía un límite establecido.

Otros proyectos \cite{HASMonitSec2:2017} ampliaron este planteamiento incluyendo un sensor MQ-2 para la detección de fugas de gases peligrosos para el ser humano como metano, monóxido de carbono o GLP. Para evitar errores en el aviso al usuario, se incluyó una doble vía de notificación SMS a través de un módulo GSM y una tarjeta WiFi ESP8266.

En cuanto a la integración de sistemas domóticos con cámaras de seguridad, cabe destacar propuestas \cite{HASMonitSec3:2016} como el proyecto realizado por la Universidad de Alabama, en el que se instaló un sensor PIR y una cámara web en el ordenador Raspberry Pi. Con la detección de un allanamiento por el sensor PIR, la cámara captura la foto de la persona que ha accedido de manera irregular a la vivienda y se procede al envío de la misma en un email además de su almacenamiento web en un servidor. De esta forma tanto el email como la imagen desplegada en el sitio web, pueden ser accesibles por cualquier usuario a través de un dispositivo móvil, tablet o PC.

\vfill
\pagebreak

\section{Integración de la domótica en \textit{Internet of Things}}

Desde que en 1985 Peter T Lewis acuñase el concepto \textit{Internet of Things} durante su charla en la Comisión Federal de Comunicaciones (FCC) de EEUU \cite{HASIoT:2017}, no se esperaba el impacto social y tecnológico que provocaría en la actualidad y que se prevé para el futuro.

Se puede definir IoT o \textit{Internet of Things} como la red de objetos físicos conectados que pueden comunicarse e intercambiar datos entre sí sin necesidad de la intervención del ser humano \cite{HASIoT:2017}. 
Todo objeto físico que pueda ser provisto de un dispositivo electrónico (sensores, actuadores, tarjetas de comunicación, ...) y una dirección IP para permitir el flujo de datos entrante y saliente a través de internet, puede formar parte de un sistema IoT. Es por ello, que el ámbito en el que IoT puede recopilar información es muy diverso: doméstico, transporte, edificios e incluso naturaleza.

En este escenario, resulta complicado hablar de entornos aislados, si no más bien de un sistema global en el que cada objeto puede desencadenar resultados fuera de su ámbito teórico de acción. 

Como ejemplo ilustrativo podríamos considerar una persona que se dirige en vehículo a su vivienda después del trabajo.
El sistema de geolocalización del automóvil unido al servicio de información del tráfico, determina que restan 20 minutos para que el usuario llegue a su destino y mediante una petición al servidor, se comunica con el sistema de calefacción para regular la temperatura de confort en la vivienda. 

Cuando el vehículo se sitúa ante el garaje, éste abre su puerta de manera automática gracias a un escáner biométrico situado en el interior del vehículo, que verificará y autenticará la identidad del usuario. Por su parte, el sistema de seguridad cambiará el modo de funcionamiento para evitar falsos positivos.
Al mismo tiempo las luces del garaje se encenderán, y a medida que el usuario se interne en el interior de la vivienda, los sensores de movimiento se encargarán de detectar la presencia del usuario y solicitar el encendido o apagado de luces para ahorrar energía.
\\

Centrándonos en la arquitectura \cite{HASIoTReview:2018}, existen pequeñas diferencias entre las distintas soluciones del mercado, no obstante en términos generales la mayoría de sistemas IoT domóticos se construyen sobre cuatro capas principales: sensores y actuadores, gateway, servidor en la nube y aplicación cliente.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.42\textwidth]{figuras/estadoarte/arduinomega.png}
	\caption{Placa Arduino Mega}{\protect\footnotemark}
	%\caption*{Fuente: Arduino}
	%\caption*{https://www.arduino.cc/en/main/boards}
	\label{fig:arduinomega}
\end{figure}
	\footnotetext{Fuente: Arduino, URL: \url{https://www.arduino.cc/en/main/boards}}


\vfill
\pagebreak

En primer lugar los sensores y actuadores, están encargados de recopilar datos y ejecutar acciones físicas respectivamente. Éstos se conectan a microcontroladores (figura \ref{fig:arduinomega}) que digitalizarán los datos obtenidos por los sensores y transmitirán las señales a los actuadores. Esta unión constituye el dispositivo IoT.

El flujo de información entre todos los dispositivos IoT y el servidor alojado en la nube, se canaliza a través de un dispositivo pasarela o \textit{gateway}, posibilitando el acceso a Internet a los dispositivos IoT y segurizando las comunicaciones vía API.



En cuanto al servidor, se compone de tres niveles: 

\begin{itemize}
	\item Una aplicación \textit{back-end}, donde se procesa y analiza la información enviada por el \textit{gateway}, se realiza la toma de decisiones y se envían las respuestas a los dispositivos IoT, de nuevo a través del \textit{gateway}.
	\item Una BBDD, para almacenar los datos recibidos de todos los dispositivos IoT.
	\item Una aplicación \textit{front-end}, para permitir al usuario gestionar los dispositivos IoT conectados.
\end{itemize}

Por último, la gran mayoría de sistemas IoT proporcionan aplicaciones móviles para facilitar al usuario el control y configuración del sistema de manera remota, por medio de una UI \cite{Jabbar:2018}.

Como ejemplo característico de esta arquitectura, cabe destacar el proyecto \cite{HASIoT2:2016} desarrollado por la Universidad Politécnica de Bucarest. El planteamiento cosiste en un sistema domótico IoT en el que emplean un sensor LDR junto con bombillas Philips Hue para el control automático y manual de la iluminación del hogar, un termostato Nest para controlar la calefacción y por último un sensor de movimiento junto con un zumbador y un motor para alertar y cerrar de manera automática la puerta de la vivienda en caso de intento de allanamiento.
Gracias al protocolo \textit{IoTivity} se gestiona la comunicación entre sensores y actuadores con el dispositivo \textit{gateway}, independientemente del fabricante del producto. A diferencia del presente proyecto, se empleó como dispositivo gateway la placa Intel Edison, que en líneas generales posee mejores prestaciones que Raspberry Pi, pero su coste es sustancialmente mayor.

\textit{IoTivity} además ofrece un API framework basado en arquitectura REST para establecer la comunicación entre el dispositivo gateway y el servidor. Por su parte, el servidor IoTivity no sólo se encarga del almacenamiento y análisis de información, sino que además permite identificar de manera unívoca cada dispositivo conectado a la red del sistema.
\\


Teniendo en cuenta que las predicciones de dispositivos IoT conectados para 2025 será de 21,500 millones\footnote{Datos extraídos de \textit{IOT Analytics }\url{https://iot-analytics.com/}}, un aumento del 250\% frente al año 2019, resulta lógico pensar que tal cantidad de datos recibidos y enviados de forma continua a servidores en la nube, conduce a problemas de almacenamiento de datos y consumo excesivo de ancho de banda.

Para solucionar estos problemas, surge el concepto \textit{Edge Computing} \cite{Chakraborty:2017} o Procesamiento en el Borde, en referencia a la frontera intangible que existe entre el dispositivo gateway y el servidor. \cite{HASIoTReview:2018} 	Su objetivo se centra conseguir que los datos generados por los dispositivos IoT se procesen más cerca de donde se crearon, en lugar de enviarlos y que se procesen en servidores en la nube o centros de datos. 

\vfill
\pagebreak

El propio \textit{gateway} u otros dispositivos hardware con capacidad de procesamiento, efectúan la toma de decisiones, que de otro modo sería asumida por el servidor. Con esta propuesta se consigue que los dispositivos IoT obtengan las respuestas casi en tiempo real y que se filtren los datos a enviar al servidor, para evitar sobrealmacenamientos en la BBDD. Para una mejor comprensión, se ilustra en la figura \ref{fig:iotarquitectura} el ejemplo de una arquitectura IoT basada en el concepto \textit{Edge Computing}.


\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/estadoarte/iotarquitectura.png}
	\caption{Arquitectura estándar IoT}{\protect\footnotemark}
	%\caption*{Fuente: Quora}
	%\caption*{https://www.quora.com/What-is-IoT-architecture}
	\label{fig:iotarquitectura}
\end{figure}
	\footnotetext{Fuente: Quora, URL: \url{https://www.quora.com/What-is-IoT-architecture}}






