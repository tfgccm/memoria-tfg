\chapter{Diseño de la Arquitectura}

A lo largo de este capítulo se presentará al lector el diseño de la arquitectura hardware y software del proyecto, indicando las razones que motivaron su elección. 

\section{Introducción}

La toma de decisiones, a la hora de definir la arquitectura del proyecto, ha sido sin duda una de las fases más duras e importantes. Como puede apreciarse en el diagrama de Gantt del proyecto (figura \ref{fig:gantt}), se han destinado dos meses a la investigación de propuestas tecnológicas que ayudasen a definir el diseño.

Como es lógico pensar, la primera fase de un proyecto es de vital importancia, puesto que un pequeño error en el diseño inicial puede desencadenar problemas de difícil subsanación en el futuro. Aunque suponga una gran inversión de tiempo y esfuerzo, probar otras opciones ha conducido a una solución más robusta y a un mejor conocimiento de la materia.

El análisis de propuestas inicial no se limita únicamente a elegir un modelo de microcontrolador determinado o un lenguaje de programación, sino conocer de primera mano cómo se orquestan los proyectos domóticos del mercado y tomarlos como referencia técnica.


\vfill
\pagebreak

\section{Arquitectura Hardware}

Como se ha visto a lo largo del capítulo  \textbf{\ref{chap:estadodelarte}. Estado del Arte}, la mayoría de productos IoT poseen una arquitectura ordenada en cuatro niveles: sensores y actuadores, gateway, servidor en la nube y aplicación cliente. Por este motivo, la arquitectura elegida para el proyecto se diseñó de manera semejante, con algunas diferencias que se matizarán más adelante.

La figura \ref{fig:arqiotdiag} ilustra de forma gráfica la arquitectura estándar de un sistema domótico basado en IoT. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/disenyo/arquitecturaiotdiagrama.png}
	\caption{Arquitectura estándar de domótica IoT}
	\label{fig:arqiotdiag}
\end{figure}

De manera inmediata se observa que los dispositivos IoT, sensores y actuadores, tienen un sistema integrado de control y comunicación inalámbricos. Si nos adentramos en su estructura interna, están formados por:

\begin{itemize}
	\item Microcontrolador para recibir los datos obtenidos por los sensores, enviar señales a los actuadores y comunicarse con el transceptor.
	\item Transceptor para realizar la recepción y transmisión de las comunicaciones.	
	\item Uno o varios sensores y/o actuadores.	
	\item PSU alimentada por una batería.
\end{itemize}

Reproducir esta configuración incurriría en un alto coste, ya que sería necesario proporcionar a cada sensor un microcontrolador, un \textit{Shield Xbee} para conseguir una comunicación inalámbrica Zigbee con el dispositivo gateway, y una alimentación independiente. Por este motivo, se ha simplificado esta distribución empleando una sola placa microcontroladora a la cual se conectarán todos los sensores y actuadores del sistema.


En la figura \ref{fig:arqsidocsdiag} se ilustra un diagrama descriptivo que detalla la arquitectura diseñada e implementada en este proyecto. Claramente se distingue la separación en dos bloques de los dispositivos IoT, siendo Arduino el único microcontrolador. 
De entre las múltiples opciones del mercado, finalmente se ha elegido Arduino como placa microcontroladora ya que tiene un número de puertos analógicos y digitales que cubren las necesidades del proyecto, su coste es reducido y ofrece una interfaz de comunicación USB de fácil implementación.

Aunque la comunicación entre Arduino y el dispositivo gateway se establezca por puerto USB, se ha planteado como posible evolutivo futuro, la migración a una comunicación inalámbrica entre ambos.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/disenyo/arquitecturasidocsdiagrama.png}
	\caption{Diagrama de la arquitectura SIDOCS}
	\label{fig:arqsidocsdiag}
\end{figure}

En cuanto al dispositivo gateway del proyecto, se ha elegido el ordenador Raspberry Pi 3. Gracias a su reducido tamaño, bajo coste y gran versatilidad, posibilita un canal seguro de comunicación entre los dispositivos de campo y el servidor. 
Tomando los principios de \textit{Edge Computing}, se han delegado en él ciertas tareas de análisis de datos que evitan el excesivo uso de la red y el almacenamiento innecesario en la BBDD.

Para dotar de acceso global al servidor del proyecto, se barajaron múltiples alternativas: desde instalar un servidor en mi vivienda hasta emplear una plataforma IoT como \textit{Altair Smartworks} (antes \textit{Carriots}). 

Sin embargo, la solución más acertada era un servicio gratuito de alojamiento web, que ofreciese un certificado SSL, para que todo el tráfico de peticiones con el servidor fuera seguro. Teniendo en cuenta que el \textit{framework } a desarrollar, \textit{Django Rest Framework}, está basado en Python, la plataforma \textit{PythonAnywhere} fue la opción escogida, ya que adicionalmente ofrece un IDE online y un panel de control amigable con gran capacidad de configuración.
\\
 
Es muy importante señalar que a pesar de que el diseño propuesto tiene unas características similares a una arquitectura IoT, no es completamente fiel a sus estándares. Por el contrario, mi objetivo ha consistido en diseñar una arquitectura domótica, tomando algunas características de los sistemas IoT y de otros sistemas domóticos para cubrir las necesidades y requisitos del proyecto.

\vfill
\pagebreak

\section{Arquitectura Software}

Tras definir la arquitectura hardware, se ha planteado el diseño software que sustentará la lógica y el flujo de operaciones del sistema. Se distinguen cuatro partes bien diferenciadas: un programa de control para la placa Arduino, un programa de comunicación y control para el dispositivo gateway Raspberry Pi 3, una aplicación API REST servidor y una aplicación móvil Android. Dada la particularidad que engloba el proceso de comunicación serie entre Arduino y Raspberry Pi, se hará una mención especial al diseño del proceso de envío y recepción de mensajes entre ambas plataformas.  

\subsection{Programa Arduino}
\label{ssc:programaarduino}
%En la placa Arduino se ha desarrollado un programa encargado de controlar todos los sensores y actuadores del sistema, así como la sincronización y comunicación por puerto USB con el dispositivo central gateway.

Para favorecer la reutilización, escalabilidad y el encapsulamiento del código en el programa de control Arduino, se ha basado su diseño de programación en POO. Dado que el lenguaje de programación de Arduino, \textit{Processing/Wiring}, consiste en una versión simplificada de C++, se han organizado los archivos de cabecera y código fuente de cada clase en una carpeta ''include'' y ''src'', respectivamente.

Con el propósito de facilitar las tareas de diseño, se trazó un diagrama de clases, que partiendo de una estructura básica, se fue completando paulatinamente con los atributos y métodos requeridos (ver figura \ref{fig:disenyoarduino}).


\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/disenyo/umlarduino.png}
	\caption{Diagrma de clases UML Arduino}
	\label{fig:disenyoarduino}
\end{figure}

\vfill
\pagebreak

Centrándonos en el diseño, se observa que cada tipo de sensor y actuador del sistema dispone de una clase independiente para su gestión, así se consigue una clara distinción de responsabilidades por clase. 
Por este motivo, se hizo uso de una clase base ''Detector'' de la que heredan todas las clases asociadas a los sensores de detección: movimiento, inundación y gas. Como ya se ha mencionado, esta estructura típica en POO, permite organizar y reutilizar gran cantidad de código.

Para agrupar todos los componentes del sistema, se ha incluido la clase general ''SistemaSeguridad''. En ella se canalizan todos los procesos de inicialización, lectura, evaluación de datos, ejecución de acciones y comunicación serie. Además se consigue brevedad y una fácil lectura del archivo principal de Arduino, \textit{SIDOCS.ino}, pues tan sólo es necesario instanciar un único objeto para ejecutar todo el proceso.

\subsection{Programa Gateway}
\label{ssc:programagateway}

Antes de proceder al desarrollo del programa, se eligió como lenguaje de programación \textit{Python}, puesto que ofrece una excelente integración con Raspberry Pi y cuenta con gran cantidad de librerías externas de código abierto para ampliar las funcionalidades del programa.

De acuerdo a los estándares de Python, el diseño del programa se ha estructurado en cinco módulos, representados cada uno por un archivo \textit{.py}: communications, models, gateway, log y config. 
\\

En primer lugar, en el módulo \textbf{communications} se encuentran las clases encargadas de proporcionar al programa una vía de comunicación REST con el servidor. Más concretamente, dota al sistema de los métodos y atributos necesarios para conectar y autenticar el programa con el servidor y realizar las operaciones CRUD requeridas. 

Como ya se ha mencionado, la seguridad en la comunicación gateway-servidor-cliente es primordial, y por este motivo durante el diseño del programa gateway y la aplicación servidor API REST, se estableció como requisito del sistema un método de autenticación flexible y con buena integración en dispositivos móviles. En este caso se optó por emplear el estándar \textit{JSON Web Tokens}, cuyos detalles se expondrán en profundidad en el capítulo \ref{sec:apirest}.


Por otra parte, el módulo \textbf{models} establece un mapeo en forma de clases, entre los datos obtenidos de Arduino y el modelo de datos definido en el servidor. Cabe resaltar, que el diseño del modelo de datos del servidor y la estructura de clases del módulo ''models'' se realizó de manera conjunta, para evitar problemas de inconsistencia de datos en el futuro.


El módulo \textbf{gateway} es el principal del programa. De forma equivalente al diseño del programa Arduino, contendrá una sola instancia de la clase general del modelo de datos, ''Sidocs'', y ejecutará en cuatro hilos independientes los procesos de: 

\begin{itemize}
	\item Flujo de comunicaciones con Arduino y el servidor. 
	\item Autenticación con el servidor y obtención de token JWT.
	\item Refresco de tokens JWT tras su expiación.
	\item Consulta de la IP pública de Raspbery Pi y envío al servidor, para mantener actualizada la URL de acceso al vídeo streaming de la cámara de vigilancia.
\end{itemize}


Por último se encuentran los módulos \textbf{log} y \textbf{config} cuyo ámbito de acción es transversal a toda la aplicación. 

El módulo \textbf{log} recogerá todos los eventos relevantes del proceso (conexiones, actualización de parámetros, errores,...) y los trazará en ficheros de texto, indicando su fecha de registro y descripción. Esto es especialmente valioso cuando se desea realizar una auditoría del sistema o se necesita depurar errores.

Finalmente, el módulo \textbf{config} contiene todos los parámetros de configuración del programa: URL del servidor, credenciales de autenticación en el servidor, ruta de carpeta de logs, etc. Al emplearse referencias comunes a estos parámetros en todo el código, se evitan posibles discrepancias de configuración entre módulos.
\\

Estableciendo esta distribución de clases y módulos se ha conseguido mantener una separación de responsabilidades por funcionalidad en el proceso, de forma que si en el futuro se quisiese ampliar el número de dispositivos o implementar nuevas funcionalidades, los cambios necesarios a aplicar serían mínimos.

Para una mejor comprensión, en la figura \ref{fig:disenyogateway} muestra un diagrama de clases UML donde se distingue de manera clara tanto la asociación de clases como la distribución de módulos.
\\


\vfill
\pagebreak

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.5\textwidth,angle=90,origin=c]{figuras/disenyo/umlgateway.png}
	\caption{Diagrma de clases UML Gateway}
	\label{fig:disenyogateway}
\end{figure}

\subsection{Comunicación Arduino - Raspberry Pi}
\label{ssc:comardurb}

Uno de los mayores retos a los que se enfrentaba este tipo de arquitectura, era la definición de un sistema de comunicación coordinado y escalable entre Arduino y Raspberry Pi. 

Hay que tener en cuenta que los métodos de lectura y escritura por puerto serie para estas dos plataformas se basan la transmisión de cadenas de bytes. Es por este motivo que se ha diseñado un sistema intercambio de mensajes, compuestos por cadenas de caracteres codificados bajo el estándar UFT-8. Aunque en el capítulo \ref{chap:software} se ahondará en este tema, se ha realizado un diagrama explicativo que aporta una perspectiva más clara del formato definido y las etapas que tienen lugar.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.85\textwidth]{figuras/disenyo/conexarduinoraspberry.png}
	\caption{Diagrama de comunicación Arduino - Raspberry Pi}
	\label{fig:conexarduinoraspberry}
\end{figure}

Atendiendo al diagrama expuesto, se pueden extraer cuatro fases principales:

\begin{enumerate}
	\item \textbf{Sincronización inicial}: Arduino enviará el carácter ''\textit{\textbf{S}}'', que tras ser procesado por el programa Gateway, ambas plataformas estarán en disposición de iniciar el bucle de comunicación.
	\item \textbf{Envío de parámetros a Arduino}: De forma genérica el programa gateway comprobará en cada ciclo, mediante consultas \textit{GET} al servidor, si alguno de los parámetros que configura el usuario Android ha sido modificado. De ser así, recogerá su valor y lo enviará a Arduino para que actualice sus variables internas y opere en consecuencia. 

\vfill
\pagebreak
	
	En este punto es preciso mencionar que aunque la comprobación se realice únicamente sobre el modo de funcionamiento, en futuros desarrollo podrían ampliarse al envío de grados de giro del servomotor que maneja la cámara de seguridad, peticiones de encendido o apagado de bombillas, etc. 
	
	El formato de cadena empleado para el envío de parámetros es ''\textbf{\textit{P,Mm\$}}'' cuyos miembros se describen a continuación:
	
		\begin{itemize}
		\item La letra ''P'' indica que se están enviando parámetros modificados por el usuario Android.
		\item La letra ''M'' indica que el parámetro que se está leyendo en el mensaje es ''Modo de Funcionamiento''.
		\item De forma genérica se ha representado con la letra 'm' el valor del modo de funcionamiento. Dicho valor puede variar entre 1 (Completo), 2 (En Casa) o 3 (Desactivado).
		\item El carácter ''\$'' delimita el final del mensaje. Por lo que a partir de su lectura, no se continuará segmentando la información recibida.
		\item El carácter '','' ayuda a separar cada uno de los parámetros que componen el mensaje.
		\end{itemize}

	\item \textbf{Petición de datos por Raspberry Pi}: Una vez que el programa Gateway está preparado para recibir los datos recogidos por Arduino, enviará el carácter ''\textit{\textbf{D}}'' por puerto serie.
	
	\item \textbf{Envío de datos por Arduino}: Tras recibir el aviso de recepción de datos por parte de Raspberry Pi, Arduino compondrá la cadena de datos con los valores de temperatura y humedad relativa leídas, así como del estado de las alarmas evaluadas en función del modo de funcionamiento.
	
	De forma semejante a la actualización de parámetros, el formato para la cadena de datos enviada es ''\textit{\textbf{T,tt.hh,H,hh.hh,A,aPIR,aGas,aAgua,\$}}'', cuyos miembros se describen a continuación:
	
			\begin{itemize}
		\item La letra ''T'' indica que el valor contiguo del mensaje corresponde a la temperatura.
		\item De forma genérica se representa con ''tt.tt'' el valor con dos decimales de la temperatura registrada con el sensor DHT22, en la presente iteración del proceso de control.
		\item La letra ''H'' indica que el valor contiguo del mensaje corresponde a la humedad relativa.
		\item De forma genérica se representa con ''hh.hh'' el valor con dos decimales de la humedad relativa leída con el sensor DHT22, en dicha iteración.
		\item La letra ''A'' indica que los siguientes tres valores del mensaje corresponden al estado de las alarmas de allanamiento, gas e inundación.
		\item De forma simbólica ''aPIR'' indica el estado de la alarma de allanamiento, cuyo valor puede ser 0 (no hay movimiento) o 1 (alarma por detección de movimiento).
		\item De igual forma ''aGas'' indica el estado de la alarma de presencia de gases peligrosos o humo, cuyo valor puede ser 0 (no hay presencia de gases) o 1 (alarma por escape de gas o incendio).
		\item De nuevo ''aAgua'' indica el estado de la alarma inundación, cuyo valor puede ser 0 (no hay inundación) o 1 (alarma por escape de agua).
		\item El carácter o token ''\$'' delimita el final del mensaje. Desde el programa Gateway se podrá controlar de manera sencilla tanto la longitud total del mensaje como la existencia del token de fin de mensaje.
		\item El carácter '','' separa cada uno de los componentes que forman el mensaje.
	\end{itemize}
	
\end{enumerate}


\vfill
\pagebreak

\subsection{Aplicación API REST}


Si partimos de la infraestructura que provee \textit{PythonAnywhere} para el alojamiento de proyectos web en la nube, se consideraron varias alternativas paralelas como \textit{Flask}, \textit{Bottle} o \textit{Falcon}, que aun siendo perfectamente válidas no ofrecían las mismas prestaciones que el finalmente escogido \textit{Django Rest Framework}. Entre las ventajas más importantes de Django Rest Framework frente a sus competidores, destaca:

\begin{itemize}
	\item Provee una arquitectura claramente definida y ordenada sobre la cual construir el proyecto.
	\item Dispone de una amplia colección clases genéricas que reducen sustancialmente la cantidad de código a implementar.
	\item Ofrece un motor ORM para mapear los campos de la BBDD con una estructura de clases basada en POO. 
	\item Ofrece una web API navegable, de gran utilidad a la hora de realizar pruebas durante el desarrollo.
	\item Existe una gran cantidad de librerías externas desarrolladas por su extensa comunidad.
	\item Recientemente han publicado el proyecto \textit{Django Channels} para la integración de \textit{Django} y \textit{Django Rest Framework} con WebSockets, y conseguir así un canal de comunicación en tiempo real.
\end{itemize}

En la figura \ref{fig:disenyodrfstandar}, se muestra una arquitectura estándar de un proyecto Django Rest Framework, formada por cuatro capas principales: URL, View, Serializer y Model.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/disenyo/umldrfstandar.png}
	\caption{Arquitectura estándar Django Rest Framework}
	\label{fig:disenyodrfstandar}
\end{figure}

\vfill
\pagebreak

Cualquier aplicación móvil o web puede realizar peticiones a la aplicación API REST por medio de una \textbf{URL} específica. Esta URL indicará la ruta de acceso a un recurso o vista, por ejemplo la lectura del último valor de temperatura registrado en la BBDD o la actualización del modo de funcionamiento. 

Las posibles operaciones CRUD que pueden realizarse sobre este recurso se definirán en la capa \textbf{View} al igual que el modo de serialización a emplear.

El proceso de serialización/deserialización no es más que una traducción entre el modelo de datos del API REST a formato JSON y viceversa. La forma en la que se procesa esta traducción de datos, se alojará en la capa \textbf{Serializer}.

Por último, la capa \textbf{Model} contendrá las clases que definen el modelo de datos del sistema, estableciendo un mapeo directo con los campos y tablas de la BBDD. Es en este punto, donde el motor ORM efectuará la consulta en la BBDD y tras serializar el resultado a formato JSON, se retornará al cliente la respuesta.
\\

Aunque la estructura estándar de Django Rest Framework puede adaptarse a pequeñas aplicaciones, fue necesaria la inclusión de tres capas adicionales para conseguir las funcionalidades requeridas en el proyecto, tal y como se observa en la figura \ref{fig:disenyodrfnew}. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/disenyo/umldrfnew.png}
	\caption{Propuesta de Arquitectura Django Rest Framework}
	\label{fig:disenyodrfnew}
\end{figure}

En primer lugar se añadieron dos aplicaciones externas para integrar el proyecto con autenticación basada en \textit{JSON Web Tokens} y para el envío de notificaciones push a clientes móviles vía \textit{Firebase Cloud Messaging}.

Toda petición entrante en el servidor será previamente analizada por la capa de autenticación \textbf{JWT Auth} y se verificará que el solicitante está registrado como usuario. 

\vfill
\pagebreak

Seguidamente, es necesario comprobar si el usuario posee permisos suficientes para efectuar dicha petición, por lo que que se ha añadido una capa de permisos, \textbf{Permissions}, en el que se definen las operaciones CRUD que puede realizar cada rol. Como es de esperar, los usuarios móviles tendrán asignado un rol, ''clienteAndroid'', con una serie de permisos diferentes al rol empleado en el programa Gateway, ''clienteRP''. Un claro ejemplo es la inserción de nuevos registros de temperatura en la BBDD que sólo debe estar disponible para el usuario Raspberry Pi, mientras que la edición del modo de funcionamiento debe ser controlada por el usuario Android.

Finalmente, para gestionar el envío de notificaciones FCM y email a los usuarios tras producirse una alarma, se ha agregado la capa \textbf{Services}. Cuando el usuario Raspberry Pi envía una alarma al servidor, se comprueba el tipo de alarma en la capa View, para finalmente configurar y efectuar los envíos desde la capa Services.	 


\subsection{Aplicación Android}
\label{ssc:disenyoandroid}

El dominio incontestable de Android como sistema operativo móvil con la mayor cuota de mercado a nivel mundial (en torno al 86,8\%\footnote{Datos extraídos de IDC \url{https://www.idc.com/promo/smartphone-market-share/os}} en 2018), decantó decisivamente su elección como plataforma para el desarrollo de la aplicación móvil. No obstante, se valora como evolutivo futuro, la migración de la aplicación a un sistema multiplataforma como \textit{Xamarin}.

La necesidad de ofrecer al usuario una UI con usabilidad y navegabilidad intuitiva, obligan a encarar el desarrollo de la aplicación Android de manera distinta al resto de programas software del proyecto. Antes de comenzar, se ha tomado como modelo de planificación las etapas típicas de los proyectos de desarrollo web y movilidad: 

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/disenyo/fasesandroid.png}
	\caption{Fases de ejecución - Aplicación Android}
	\label{fig:fasesandroid}
\end{figure}

\begin{enumerate}
	\item Toma de requisitos funcionales.
	\item Diseño gráfico de la aplicación mediante maquetas.
	\item Definición de la arquitectura software. 
	\item Desarrollo software en base a los requisitos y diseños establecidos.
	\item Ejecución de pruebas funcionales para verificar los resultados.
\end{enumerate}

%\vfill
%\pagebreak

De acuerdo a la planificación expuesta, se realizaron en primer lugar varios diagramas UML para analizar funcionalmente los distintos casos de uso. 

En la figura \ref{fig:disenyoandroiduc1} se ha diseñado un caso de uso específico para una persona que aún no se ha registrado en la aplicación y por tanto no dispone de credenciales, o que teniendo ya una cuenta en el sistema aún no ha accedido a la aplicación.


\begin{figure}[!h]
	\centering
	\includegraphics[width=0.75\textwidth]{figuras/disenyo/umlandroiduc1.png}
	\caption{Diagrama de Caso de Uso - Usuario no registrado o no accedido}
	\label{fig:disenyoandroiduc1}
\end{figure}

En este contexto, las operaciones disponibles serán la de registro de usuario, y tras ello el acceso a la aplicación. Si el acceso se ha completado satisfactoriamente surge un escenario diferente, representado por el caso de uso de la figura \ref{fig:disenyoandroiduc2}. 


\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/disenyo/umlandroiduc2.png}
	\caption{Diagrama de Caso de Uso - Usuario accedido}
	\label{fig:disenyoandroiduc2}
\end{figure}


Desde el menú principal de la aplicación, el usuario puede comprobar la temperatura y humedad del hogar, consultar el histórico de alarmas, visualizar la emisión en tiempo real de la cámara de seguridad o cambiar la configuración del sistema, entre otras acciones.
\\

Tras cerrar los requisitos funcionales de la aplicación se procedió a definir su diseño gráfico. Es un paso previo fundamental antes de comenzar las labores de desarrollo ya que aunque el diseño pueda experimentar pequeños cambios durante la fase de programación, se necesita conocer de primera mano cuál es el objetivo a alcanzar, tanto funcional como gráficamente.

Para la realización de la maqueta o \textit{mockup} de la aplicación, se empleó el programa \textit{Balsamiq Mockups 3}, el cual permite crear diseños personalizados para aplicaciones móviles y web, con gran nivel de detalle.
\\

Partiendo del caso de uso visto en el diagrama \ref{fig:disenyoandroiduc1}, se diseñaron las pantallas correspondientes al registro del usuario y acceso a la aplicación (ver figura \ref{fig:mockup_acceso_registro}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\textwidth]{figuras/disenyo/androidaccesoregistro.png}
	\caption{Mockup aplicación Android - Acceso y Registro}
	\label{fig:mockup_acceso_registro}
\end{figure}


Una vez el usuario ha validado sus credenciales satisfactoriamente, accede al menú principal de la aplicación. En la parte superior de la pantalla aparece un menú adicional que alberga algunas opciones extras (ver figura \ref{fig:mockup_menu_menusup}). 


\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\textwidth]{figuras/disenyo/androidmenumenusup.png}
	\caption{Mockup aplicación Android - Menú y menú superior}
	\label{fig:mockup_menu_menusup}
\end{figure}

Es en este menú superior donde el usuario podrá consultar los créditos del proyecto o proceder al cierre de la sesión activa (ver figura \ref{fig:mockup_creditos_cerrar_sesion}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/disenyo/androidcreditoscerrar.png}
	\caption{Mockup aplicación Android - Créditos y Cerrar sesión}
	\label{fig:mockup_creditos_cerrar_sesion}
\end{figure}

\vfill
\pagebreak

En cuanto al menú principal, existen cuatro opciones de navegación: Sensores, Videocámara, Alarmas y Configuración. Como se comprobará más adelante, algunos iconos, textos y títulos cambiaron ligeramente durante la fase de desarrollo para conseguir una mejor experiencia de usuario.

Si se selecciona el botón \textbf{Sensores}, el usuario podrá consultar la temperatura y humedad relativa del hogar. 

De igual forma, pulsando el botón \textbf{Videocámara} se puede monitorizar la vivienda a través de la cámara de seguridad. En caso de fallo de conexión, el usuario puede forzar la reconexión con el servidor de vídeo streaming pulsando el botón ''Reconectar''.

El submenú de \textbf{Alarmas} mostrará un listado en forma de tarjetas, con las diez últimas alarmas generadas.


\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/disenyo/androidsenscamalarm.png}
	\caption{Mockup aplicación Android - Sensores, Vídeocámara y Alarmas}
	\label{fig:mockup_sensores_camara_alarmas}
\end{figure}

\vfill
\pagebreak

Por último, en el submenú \textbf{Configuración} se podrá activar y desactivar las notificaciones push de la aplicación, y cambiar el modo de funcionamiento del sistema según las necesidades del usuario (Completo, En Casa y Desactivado). 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\textwidth]{figuras/disenyo/androidconfiguracion.png}
	\caption{Mockup aplicación Android - Configuración}
	\label{fig:mockup_configuracion}
\end{figure}

A excepción del logo principal, las imágenes e iconos usadas en el diseño de la aplicación fueron obtenidas de la página FLATICON\footnote{Página oficial de FLATICON \url{https://www.flaticon.com/}}, la cual ofrece una completísima colección de recursos gráficos protegidos bajo licencia \textit{Creative Commons} (CC). Para cumplir con las reglas marcadas por este tipo de licencia, todas las referencias a los autores se han detallado en el submenú superior de \textbf{Créditos} .

El logo principal de la aplicación es el único recurso gráfico completamente original. Para su diseño me inspiré en las letras ''S'' y ''D'' del título, \textbf{S}I\textbf{D}OCS, y se compuso gráficamente en Autodesk AutoCAD para posteriormente colorearlo en \textit{Adobe Photoshop}, con los colores de las tres principales plataformas empleadas en el proyecto: Arduino, Raspberry Pi y Android.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.25\textwidth]{figuras/disenyo/iconosidocs.png}
	\caption{Icono del proyecto - SIDOCS}
	\label{fig:iconosidocs}
\end{figure}

Finalmente, tras concluir el diseño funcional y gráfico de la aplicación, se trazó la estructura software tomando como guía los requisitos expuestos.

Como punto de partida, cabe mencionar que todos los proyectos Android comparten una serie de componentes básicos: 

\begin{itemize}
	\item Archivo \textit{manifest.xml}, en el que se agrupa toda la configuración básica e información del proyecto.
	\item Directorio ''java'', donde se almacenan los archivos de código fuente y por consiguiente la lógica de la aplicación: actividades, servicios,... 	
	\item Directorio ''res'', en él se encuentran los archivos que definen el comportamiento de la aplicación: imágenes, iconos, colores, estilos, diseños de actividad, textos...
\end{itemize}


Partiendo de esta estructura estándar, fue necesario el desarrollo de paquetes adicionales para cubrir las especificaciones técnicas y funcionales de la aplicación.
En la figura \ref{fig:disenyoandroid} se marca en amarillo el paquete \textbf{data} que contiene el modelo de datos y métodos necesarios para configurar el cliente REST, que permitirá a la aplicación comunicarse de forma remota con el servidor. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.79\textwidth]{figuras/disenyo/umlandroid.png}
	\caption{Estructura del proyecto Android}
	\label{fig:disenyoandroid}
\end{figure}

\vfill
\pagebreak

En color naranja indica el paquete \textbf{service} que alberga el servicio FCM para la gestión de notificaciones \textit{push}.

Finalmente en azul se tiene el paquete \textbf{utils} el cual tiene encapsulados los métodos de lectura y escritura del diccionario protegido \textit{SharedPreferences} para almacenar los datos sensibles de la aplicación (código de usuario, contraseña, configuración, ...), así como la plantilla de las ventanas de carga. Al tratarse de clases y métodos que se instancian globalmente en la aplicación, se consigue una mayor limpieza del código.


\subsection{Servidor de Vídeo Streaming}
\label{ssc:serstreaming}

Para reproducir en tiempo real la imagen que captura la cámara de vigilancia, existen múltiples plataformas basadas en software libre, tales como \textit{MJPEG Streamer}, \textit{Motion}, \textit{FFmpeg} o \textit{UV4L} entre otras.
Antes de escoger una de ellas se realizó un trabajo de investigación a fin de encontrar qué tecnología se adaptaba mejor a los requisitos del proyecto:

\begin{itemize}
	\item \textit{Streaming} sobre protocolo seguro SSL/HTTPS.
	\item Accesible desde cualquier dispositivo móvil y navegador web.
	\item Acceso segurizado mediante autenticación de usuario.
	\item Baja latencia.
\end{itemize}

Con estas premisas, se efectuaron varias pruebas funcionales y técnicas, empleando las plataformas mencionadas. Finalmente se descartó \textit{ MJPEG Streamer} por los altos índices de latencia en la retransmisión, por su parte \textit{Motion} admitía autenticación pero no la reproducción sobre HTTPS, y por último \textit{FFmpeg} pese a tener un buen rendimiento ofrecía unas opciones de configuración muy reducidas.


Por este motivo \textbf{UV4L} fue la plataforma elegida, ya que además de cubrir todos los requisitos del proyecto, permite ajustar gran cantidad de parámetros e incluso dispone de un RESTful API para configurar el servidor de manera remota, desde cualquier cliente REST (por ejemplo una aplicación web o móvil).




