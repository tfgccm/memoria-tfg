\chapter{Gestión del proyecto}


\section{Planificación}

La planificación constituye un elemento fundamental en el proyecto pues permite identificar el orden de desarrollo de las tareas, y tras indicar la duración estimada para cada una de ellas, se puede conocer de manera aproximada la duración total del mismo. Es por ello que la herramienta elegida para definir la planificación del proyecto es el diagrama de Gantt. 
\\

Al tratarse de un proyecto individual, todos los detalles de capacidad están definidos para un sólo recurso:

\begin{itemize}
	\item Capacidad durante días laborables: 3 horas
	\item Capacidad durante días festivos y vacaciones: 10 horas
	\item Capacidad teórica semanal: 35 horas
\end{itemize}


Antes de comenzar la fase de investigación y desarrollo, se realizó un diagrama de Gantt con la planificación del proyecto. La estimación de cada tarea principal se valoró en base a la información disponible en aquel momento, por lo que un cierto grado de error es aceptable.

\begin{figure}
	\centering
	\includegraphics[width=1.65\textwidth,angle=90,origin=c]{figuras/gestion/ganttinicial.png}
	\caption{Diagrama de Gantt del Proyecto}
	\label{fig:gantt}
\end{figure}

\vfill
\pagebreak

\section{Metodología de trabajo}

\subsection{Almacenamiento y Control de Versiones}

Ante la gran cantidad de aplicaciones y ficheros desarrollados en el proyecto, se necesitó emplear un método de almacenamiento seguro, accesible de manera remota y con posibilidad de retomar versiones anteriores en caso de necesidad. Con esta premisa, se decidió elegir el sistema de control de versiones \textit{git}.

Git \cite{git} no sólo ofrece las funcionalidades mencionadas, si no que además registra todos los cambios realizados en cada archivo a lo largo del tiempo. Por lo que de forma sencilla, pueden identificarse las diferencias existentes entre distintas versiones de un archivo.

Gracias a la naturaleza de git, cualquier fichero es susceptible de ser versionado, de modo que todas las aplicaciones y la propia memoria del proyecto se han incluido el sistema de control de versiones.

Por otro lado, se ha optado por \textit{Bitbucket}\footnote{URL del proyecto SIDOCS en Bitbucket \url{https://bitbucket.org/tfgccm/}}  como servicio web para integrar el sistema de control de versiones, ya que su interfaz web es intuitiva y de fácil configuración, y dispone de un plan de uso gratuito.
\\

Una vez decidido el sistema de control de versiones y la plataforma web de alojamiento, se definió la estructura de organización de los ficheros. De manera jerárquica se creó en primer lugar un equipo donde alojar el proyecto ''SIDOCS'' y dentro de ese proyecto se añadirían los repositorios correspondientes a cada aplicación y la memoria (ver figura \ref{fig:git}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/gestion/git.png}
	\caption{Arquitectura del proyecto git}
	\label{fig:git}
\end{figure}

\vfill
\pagebreak

\subsection{Seguimiento del proyecto}
Dado que este proyecto es de carácter individual, no tenía sentido aplicar metodologías ágiles tales como \textit{Scrum}, ya que el tamaño mínimo aceptado para un equipo desarrollo es de tres miembros. No obstante para poder llevar un seguimiento diario del proyecto, se hacía necesario el uso de alguna herramienta ágil. Es por ello que se optó por el uso de un tablero \textit{Kanban}. 

\textit{Kanban} \cite{kanban} es una herramienta para la gestión ágil de proyectos, cuyo origen se remonta a los años 40, cuando la empresa japonesa Toyota orientó la producción de coches hacia un sistema de arranque. De esta forma los productos intermedios disponían de tarjetas, que detallaban requisitos de fabricación y montaje a estaciones de trabajo predecesoras. Ya en el siglo XXI, expertos del desarrollo software identificaron los beneficios que Kanban podía ofrecer en los productos y servicios que desarrollaban.
\\

Un tablero Kanban estándar se compone de tres columnas: ''Pendiente'', ''En proceso'' y ''Hecho''. En la primera columna, ''Pendiente'', se indican aquellas tareas que aún no se han realizado, posteriormente se arrastrarán hasta la columna ''En proceso'' las tareas que vayan a realizarse, y una finalizada se arrastrará hasta la columna ''Hecho''. 
Para evitar acumulación excesiva de tareas en un estado, existe el término WIP, iniciales de \textit{''Work In Progress''}. Se suele emplear con dos objetivos: establecer un límite máximo de tareas que pueden llegar a acumularse en un estado determinado e identificar de manera sencilla posibles cuellos de botella.

Uno de los puntos clave en este planteamiento, era el uso de un tablero Kanban con posibilidad de acceso global a través de Internet. De esta forma el tablero podría ser actualizado independientemente de donde se estuviera trabajando. Es por este motivo que se optó la plataforma \textit{Trello} \footnote{URL tablero Kanban del proyecto \url{https://trello.com/b/NxoKsGY5/tfg}}, la cual ofrece una interfaz web de tarjetas para crear un tablero Kanban personalizado.

En este caso particular, se ha indicado un WIP de valor tres en la columna ''En proceso'', lo cual significa que tres, es el número máximo de tareas simultáneas que pueden estar en desarrollo.

Cada tarea dispone de un título y un campo descripción donde se explican los detalles de la misma. Adicionalmente, para distinguir las diferentes partes que componen el proyecto software, se han etiquetado cada una de las tareas según su pertenencia a la arquitectura (ver figura \ref{fig:etiquetaskanban}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.4\textwidth]{figuras/gestion/tareaetiquetas.png}
	\caption{Etiquetas Kanban}
	\label{fig:etiquetaskanban}
\end{figure}

\pagebreak
A continuación (figura \ref{fig:tablerokanban}) se muestra una captura general del tablón Kanban empleado:

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/gestion/kanban.png}
	\caption{Tablero Kanban}
	\label{fig:tablerokanban}
\end{figure}

Como puede apreciarse, a las columnas estándar se ha añadido una adicional, ''Descartado'' donde se indican aquellas tareas que inicialmente se incluyeron en el proyecto, pero tras un análisis más detallado no se ha procedido a su desarrollo (incompatibilidad con otras tareas, problemas de rendimiento, solución poco eficiente, etc).

\vfill
\pagebreak

\section{Presupuesto}

En esta sección se recoge el presupuesto del proyecto (ver tabla \ref{tab:tabla_presupuesto}) donde se indica para cada elemento empleado el precio unitario, cantidad y coste total. 
\begin{table}[htbp]
	\caption{Presupuesto Material}
	\label{tab:tabla_presupuesto}
	\begin{center}
		\begin{tabular}{|c||c||c||c|}
			\hline
			\textbf{Descripción} & \textbf{Coste unitario(€/ud.)} & \textbf{Cantidad (uds.)} & \textbf{Coste total (€)} \\
			\hline
			\hline
			Arduino UNO & 20 & 1 & 20\\
			\hline
			Raspberry Pi 3 B+ & 35 & 1 & 35\\
			\hline
			Carcasa Raspberry Pi & 16 & 1 & 16\\
			\hline
			Cámara Pi NoIR V2 & 25 & 1 & 25\\
			\hline
			Cable de Cinta Pi NoIR & 3,30 & 1 & 3,30\\
			\hline
			Focos LED IR & 2,80 & 1 & 2,80\\
			\hline
			Disipador para focos LED IR & 0,33 & 2 & 0,66\\
			\hline
			Soporte de cámara & 1,20 & 1 & 1,20\\
			\hline
			Sensor DHT22 & 8,80 & 1 & 8,80\\
			\hline
			Sensor PIR & 1,12 & 1 & 1,12\\
			\hline
			Sensor MQ2 & 10,53 & 1 & 10,53\\
			\hline
			Sensor de Agua & 5,10 & 1 & 5,10\\
			\hline
			Zumbador & 4,55 & 1 & 4,55\\
			\hline
			Protoboard & 2,30 & 1 & 2,30\\
			\hline			
			Diodo LED & 0,02 & 3 & 0,06\\
			\hline			
			Cables de puente & 0,02 & 29 & 0,58\\
			\hline			
			Resistencia \SI{330}{\ohm} & 0,01 & 3 & 0,03\\
			\hline
			Resistencia \SI{4,7}{k\ohm} & 0,01 & 1 & 0,01\\
			\hline				
			Tablero de madera (600x400)& 3,95 & 1 & 3,95\\
			\hline				
			Tablero de cartón pluma (600x400)& 2,95 & 2 & 5,90\\
			\hline				
			Hoja de metacrilato transparente& 2,00 & 1 & 2,00\\
			\hline				
			Bloque de Poliestireno expandido& 0,10 & 1 & 0,10\\
			\hline				
			Pegamento para maquetas& 3,50 & 1 & 3,50\\
			\hline
			\hline
			- & - & IVA (21\%) & 32,02 \\
			\hline
			- & - & Total & 152,49\\
			\hline
		\end{tabular}
	\end{center}
\end{table}
